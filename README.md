# Homie SMD5050 led strips
Control a RGB Led Strip with MQTT messages, using Homie library. Locally compiled on a NodeMCU

Supports 2 differents sub topics :
- mode ("static", "cycle"). 
- color (RGB)

## Lib dependencies
I'm using PlatformIO for development, and the following dependencies are needed (you may use to install more libs if using Arduino IDE) :

    lib_deps = Homie, RGBLEDBlender

I make an extensive use of the excellent Homie framework (https://github.com/marvinroger/homie-esp8266/)
