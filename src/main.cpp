#include "Arduino.h"
#include "Homie.h"
#include "RGBLEDBlender.h"

// Homie Firmware
#define CUSTOM_FIRMWARE_NAME "smd5050-ledstrips"
#define CUSTOM_FIRMWARE_VERSION "1.0.0"

// Led parameters
#define COLOR_MIN (uint8_t)0
#define COLOR_MAX (uint8_t)255

// Led settings
#define FX_MODE_STATIC_NAME "static"
#define FX_MODE_CYCLE_NAME "cycle"
#define FX_MODE_STATIC 0
#define FX_MODE_CYCLE 1
#define FX_MODE_CYCLE_SPEED 2000 // in ms

#define RED_PIN D3
#define GREEN_PIN D4
#define BLUE_PIN D5

// Led management
RGBLEDBlender led_blender(RED_PIN, GREEN_PIN, BLUE_PIN);

// Homie nodes 
HomieNode led("led", "RGB-LED-STRIP"); 

// Homie settings. All optional (see setDefaultValue in setup function)
HomieSetting<const char*> defaultMode("defaultMode", "Default Mode - static or cycle");
HomieSetting<const char*> defaultColor("defaultColor", "Default color when starting in static mode, format as 'R, G, B' (ex 0,0,255)");

// Var needed to signal a change of value in the different channels. We can't update the new value in the
// Handler since it runs in a separate Thread with Homie... We need buffer variables to push the change in the main loop.
uint8_t runtimeMode; 
uint8_t runtimeColorR;
uint8_t runtimeColorG;
uint8_t runtimeColorB;

bool modeHasChanged, colorHasChanged;

// Splits a String based on a separator character
String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

// Request a mode change, alternate between cycle or static
bool modeHandler (const HomieRange& range, const String& value) {
    modeHasChanged = true;
    
    if(value.equals(FX_MODE_STATIC_NAME)) {
        runtimeMode = FX_MODE_STATIC;
    } else if(value.equals(FX_MODE_CYCLE_NAME)) {
        runtimeMode = FX_MODE_CYCLE;
    }
    Homie.getLogger() << "Mode changed to " << value << endl;
    return true;
}

void updateRuntimeColorsBasedOnStringRGBValues(const String& value) {
    runtimeColorR = getValue(value, ',', 0).toInt();
    runtimeColorG = getValue(value, ',', 1).toInt();
    runtimeColorB = getValue(value, ',', 2).toInt();

    // No need to check if >0 since runtimeColors are unsigned
    runtimeColorR = min(COLOR_MAX, runtimeColorR);
    runtimeColorG = min(COLOR_MAX, runtimeColorG);
    runtimeColorB = min(COLOR_MAX, runtimeColorB);

}

bool colorRGBHandler (const HomieRange& range, const String& value) {    
    colorHasChanged = true;
    Homie.getLogger() << "New RGB Color : " << value << endl;
    updateRuntimeColorsBasedOnStringRGBValues(value);
    Homie.getLogger() << "Color changed to RGB : " << runtimeColorR << "," << runtimeColorG << "," << runtimeColorB << "," << endl;
    return true;
}

void loopHandler() {    
	/***********************
     * Print colors
     ***********************/
    if(runtimeMode == FX_MODE_STATIC) {
        led_blender.Hold(Color(runtimeColorR, runtimeColorG, runtimeColorB));
    } else {
        led_blender.Random(FX_MODE_CYCLE_SPEED);
    }
    
    /***********************
     * Publish changes
     ***********************/
    if(colorHasChanged) {
        colorHasChanged = false;
        led.setProperty("color").send(String(runtimeColorR) + "," + String(runtimeColorG) + "," + String(runtimeColorB));
    } 

    if(modeHasChanged) {
        if(runtimeMode == FX_MODE_STATIC) {
            led.setProperty("mode").send(FX_MODE_STATIC_NAME);
        } else {
            led.setProperty("mode").send(FX_MODE_CYCLE_NAME);
        }
    }
}

void ledSetupHandler(){
    updateRuntimeColorsBasedOnStringRGBValues(defaultColor.get());    
}

void setup() {
    Serial.begin(115200);
    Homie.getLogger() << endl << endl;
    Homie_setFirmware(CUSTOM_FIRMWARE_NAME, CUSTOM_FIRMWARE_VERSION);

    // Setup topics    
    led.advertise("mode").settable(modeHandler);    
    led.advertise("color").settable(colorRGBHandler);

    // Default values
    defaultMode.setDefaultValue(FX_MODE_STATIC_NAME);
    defaultColor.setDefaultValue("0,0,0")    ;

    // End of setup
    Homie.setSetupFunction(ledSetupHandler);
    Homie.setLoopFunction(loopHandler);
    Homie.setup();
}

void loop() {
    Homie.loop();
}